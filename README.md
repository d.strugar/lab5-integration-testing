# Lab 5

[link to spreadsheet with results](https://docs.google.com/spreadsheets/d/1InLSZLXTfCwGyACciupQS3jmEKutsUa0Q5e-_d4nVO0/edit?usp=sharing)

## Bugs

1. incorrectly return ok when (either one of: time, distance, estimated time,
   estimated distance = 0)
2. price per km is not correct (spec says 10, but answers suggest it's 12.5)
3. if driver deviates more than allowed, price per minute gets to 16.6 instead
   of 21 as spec indicates
4. allowed deviation is not 16% as in spec, it must be 21%

## Equivalence Classes

| input            | value                     |
| ---------------- | ------------------------- |
| distance         | <=0, >0                   |
| type             | budget, luxury, else      |
| plan             | fixed_price, minute, else |
| planned distance | <=0, >0                   |
| planned distance | <=0, >0                   |
| time             | <=0, >0                   |
| planned time     | <=0, >0                   |
| discount         | yes, no, else             |

## Specs

| input           | value |
| --------------- | ----- |
| budget per min. | 21    |
| luxury per min. | 60    |
| fixed per km.   | 10    |
| allowed dev. %  | 16    |
| discount %      | 16    |

## BVA and Test Cases

They are made and highlighted in the
[google sheet](https://docs.google.com/spreadsheets/d/1InLSZLXTfCwGyACciupQS3jmEKutsUa0Q5e-_d4nVO0/edit?usp=sharing)
also linked above.

## Conclusions

Essentially, I tried all **Invalid Request** inputs, and most of them returned
the desired result. First I noticed that providing 0 values for most of the
inputs does not generate errors, and I found that behavior not to be acceptable.
For example, a driver can drive for 1km but for 0 mins, and that does not
generate an error. This is outlined in **Bug#1**.

Next, I tried plugging in values and saw that the fixed price per kilometer is
incorrect. Instead of it being 10, as in the specifications, it yielded 12.5
when I worked out the math. This is indicated in **Bug#2**.

Furthermore, I noticed that in the fixed price plan when the deviation goes
above the desired margin, price per minute is 16.6 and not 21 as spec dictates
(**Bug#3**).

Lastly, the deviation is not 16% as in the spec, but I worked it out to be 21%.
After 21% (i.e. >=22% or >21%) it starts giving the minute plan, instead of
starting at 17% (**Bug#4**).
